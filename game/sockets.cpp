#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <time.h>
#include <math.h>
#include <curses.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

void fin_juego(int client_fd) {
    bool * mensaje = (bool *) malloc(count * sizeof(bool));
    read(client_fd, texto, count * sizeof(bool));
    if(!mensaje)
        printf("El jugador ha perdido\n");
    else
        printf("El jugador ha ganado\n");
    close(client_fd);
}

int main(){
        int sock_fd = socket(AF_INET,SOCK_STREAM,0);

        struct sockaddr_in addr;

        memset(&addr, 0, sizeof(addr));

        addr.sin_family = AF_INET;
        addr.sin_port = htons(8888);
        addr.sin_addr.s_addr = INADDR_ANY;

        int error = bind(sock_fd,(struct sockaddr *) &addr, sizeof(addr));

        if(error != 0) {
                printf("Fallo al conectar con el cliente\n");
                exit(1);
        }

        listen(sock_fd, 3);

        do {
                socklen_t size = sizeof(addr);

                int client_fd = accept(sock_fd, (struct sockaddr *) &addr, &size);

                if(client_fd != -1)
                        fin_juego(client_fd);
        } while(true);
        close(sock_fd);
        return EXIT_SUCCESS;
}
