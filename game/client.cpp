#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <time.h>
#include <math.h>
#include <curses.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

#define ESC 27
#define ARRIBA 1
#define DERECHA 2
#define ABAJO 3
#define IZQUIERDA 4
#define MAX_LENGTH 20
#define SNAKE_CHARACTER '*'
#define FOOD_CHARACTER '+'
#define WALL_CHAR '='
#define MIN 1
#define POINT 30
#define N 30

int finish = 0;
int row, col;
int direccion;
int snake_length = 1;
int comida = 1;
int score = 0;
int y = 4;
int x = 2;

struct Coordenadas{
    int x;
    int y;
} snake[MAX_LENGTH], cola, nuevacola, last, last2, food, scored;

void obtener_tecla(){
    int tecla = getch();
    switch(tecla){
        case KEY_UP:
            direccion = ARRIBA;
            break;
        case KEY_DOWN:
            direccion = ABAJO;
            break;
        case KEY_LEFT:
            direccion = IZQUIERDA;
            break;
        case KEY_RIGHT:
            direccion = DERECHA;
            break;
    }
}

void drawLevel() {
    for (int i=0; i<row; i++) {
        mvaddch(i, 1, WALL_CHAR);
        mvaddch(i, col-1, WALL_CHAR);
    }
    for (int i=1; i<col-1; i++) {
        mvaddch(1, i,WALL_CHAR);
        mvaddch(row-1, i, WALL_CHAR);
    }
}

void drawScore(){
    scored.x = 0;
    scored.y = col/2.5;
    mvprintw(scored.x, scored.y, "Score: %i", score);
}

void cambiar_direccion(){

    last = snake[0];
    switch(direccion){
        case IZQUIERDA:
            snake[0].y--;
            break;
        case ABAJO:
            snake[0].x++;
            break;
        case DERECHA:
            snake[0].y++;
            break;
        case ARRIBA:
            snake[0].x--;
            break;

    }
    if(snake[1].x == snake[0].x && snake[1].y == snake[0].y)
        finish = 2;
    for(int i=1;i<snake_length;i++){
        last2 = snake[i];
        snake[i] = last;
        last = last2;
    }
}

void comprueba(){
    if(snake_length==MAX_LENGTH){
        timeout(-1);
        finish=1;
    }
    else {
        bool collidesnake = false;
        for(int i=1; i<snake_length; i++)
            if(snake[0].x == snake[i].x && snake[0].y == snake[i].y)
                collidesnake = true;


        if((snake[0].x == row) || (snake[0].y == col) || (snake[0].x == 1) || (snake[0].y == 1) || collidesnake){
            timeout(-1);
            finish=2;
        }
    }
}

void comer(){
    if((snake[0].x == food.x)&&(snake[0].y == food.y)){
        cola = snake[snake_length-1];

        nuevacola.x = cola.x+1;
        nuevacola.y = cola.y+1;

        snake[snake_length++] = nuevacola;
        score += POINT;

        timeout();
        food.y = rand()%(col);
        food.x = rand()%(row);
        dibujar_comida();
    }
}

void drawScore(){
    scored.x = 0;
    scored.y = col/2.5;
    mvprintw(scored.x, scored.y, "Score: %i", score);
}

void dibujar_snake(){

    for(int i=0 ; i<snake_length; i++){
        mvprintw(snake[i].x, snake[i].y,"%c", SNAKE_CHARACTER);
    }
}

void dibujar_comida(){
    if((food.y > 1) && (food.x > 1) && (food.y < col-1) && (food.x < row-1))
        mvprintw(food.x, food.y, "%c", FOOD_CHARACTER);
    else{

        food.y = y++;
        food.x = x++;
        mvprintw(food.x, food.y, "%c", FOOD_CHARACTER);
    }
}

void ganas(int server_fd){
    clear();
    mvprintw(row/2.5, col/2, "Fin de la partida.\n");
    mvprintw(row/2, col/2, "Has ganado.");
    refresh();
    bool mensaje = true;
    int count = strlen(mensaje);
    write(server_fd, &count, sizeof(bool));
    write(server_fd, mensaje, count * sizeof(bool));
}

void pierdes(int server_fd){
    clear();
    mvprintw(row/2.5, col/2,"Fin de la partida.\n");
    mvprintw(row/2, col/2, "Has perdido.");
    refresh();
    bool mensaje = false;
    int count = strlen(mensaje);
    write(server_fd, &count, sizeof(bool));
    write(server_fd, mensaje, count * sizeof(bool));
}

void inicio(){
    getmaxyx(stdscr, row, col);
    srand(time(NULL));
}

void estoy_jugando() {
	initscr();
    inicio();
    food.x = MIN+rand()%(row-MIN);
    food.y = MIN+rand()%(col-MIN);
    noecho();
    start_color();
    init_pair(1,COLOR_YELLOW,COLOR_BLUE);
    bkgd(COLOR_PAIR(1));
    drawLevel();
    drawScore();
    keypad(stdscr, TRUE);
    curs_set(0);
    bienvenida();
    getch();
    clear();
    inicio_snake();

        while(finish == 0){
                timeout(50);
                obtener_tecla();
                clear();
                drawLevel();
                drawScore();
                cambiar_direccion();
                comprueba();
                comer();
                drawScore();
                if(!finish) {
                        dibujar_snake();
                        dibujar_comida();
                        refresh();
                }
                usleep(70000);
        }
        if(finish == 1)
                ganas(server_fd);
        else
                pierdes(server_fd);
        usleep(70000);

}

int main(){
        int sock_fd = socket(AF_INET,SOCK_STREAM,0);

        struct sockaddr_in addr;

        memset(&addr, 0, sizeof(addr));

        addr.sin_family = AF_INET;
        addr.sin_port = htons(8888);
        addr.sin_addr.s_addr = inet_addr("127.0.0.1");

        socklen_t size = sizeof(addr);

        int server_fd = connect(sock_fd, (struct sockaddr *) &addr, size);

        if(server_fd != -1)
                estoy_jugando();
        else
                printf("Fallo al cargar el juego\n");
        if (server_fd != -1)
        {
        	
        }

        close(sock_fd);
        return EXIT_SUCCESS;
}
