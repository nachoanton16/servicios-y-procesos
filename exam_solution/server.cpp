#include <assert.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>

#define UP 0
#define DOWN 1
#define R 0
#define W 1

#define GRANDE 255

void handle_client(int client_fd) {
        bool finish = false;
        do {
                int count;
                int code = read(client_fd, &count, sizeof(int));
                if (code <= 0) {
                        finish = true;
                }
                else {
                        char * texto = (char *) malloc(count * sizeof(char));
                        read(client_fd, texto, count * sizeof(char));
                        printf("READ:%s\n", texto);
                }
        } while(!finish);
        close(client_fd);
}

void limpia(int senal) {
        int codigo;
        wait(&codigo);
}

int main(int argc, char* argv[]){
        int siguiente_opcion;
        int loopback=0;
        int puerto=8888;
        int fd[2][2];
        struct sigaction sigachld;

        static const struct option long_options[] = {
                { "loopback",       1, NULL, 'l' },
                { "port",           1, NULL, 'p' },
                {  NULL,            0, NULL,  0  },
        };

        static const char* const short_options = "lp:";

        do {
                siguiente_opcion = getopt_long(argc, argv, short_options, long_options, NULL);
                switch(siguiente_opcion) {
                        case 'l':
                                loopback = 1;
                                break;
                        case 'p':
                                sscanf(optarg, "%i", &puerto);
                                break;
                        case -1:
                                break;
                }
        } while(siguiente_opcion != -1);





        memset(&sigachld, 0, sizeof(sigachld));
        sigachld.sa_handler = &limpia;
        sigaction(SIGCHLD, &sigachld, NULL);

        int sock_fd = socket(AF_INET,SOCK_STREAM,0);

        struct sockaddr_in addr;

        memset(&addr, 0, sizeof(addr));

        addr.sin_family = AF_INET;
        addr.sin_port = htons(puerto);
        if(loopback)
                addr.sin_addr.s_addr = INADDR_ANY; //CUALQUIER DIRECCION
        else
                addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK); //DIRECCION CONCRETA

        int error = bind(sock_fd,(struct sockaddr *) &addr, sizeof(addr));

        if(error != 0) {
                printf("Failed to bind\n");
                exit(1);
        }

        listen(sock_fd, 1);

        do {
                socklen_t size = sizeof(addr);

                int client_fd = accept(sock_fd, (struct sockaddr *) &addr, &size);

                if(client_fd != -1){
                        pid_t son_pid = fork();
                        if(son_pid!=0)
                                close(client_fd);
                        else{
                                close (STDIN_FILENO);
                                close (STDOUT_FILENO);

                                pipe(fd[UP]);
                                pipe(fd[DOWN]);

                                pid_t sort_id = fork();

                                if(sort_id > 0) {
                                        close(fd[UP][R]);
                                        close(fd[DOWN][W]);

                                        FILE *s_sort = fdopen(fd[DOWN][R], "r");//LECTURA

                                        int longitud;
                                        read(client_fd, &longitud, sizeof(int));
                                        int tamano = (longitud + 1) * sizeof(char);
                                        char *cadena = (char *) malloc(tamano);
                                        read(client_fd, cadena, tamano);

                                        char * texto = cadena;
                                        write(fd[UP][W], texto, sizeof(texto));
                                        free(texto);

                                        char buffer[GRANDE];
                                        fscanf(s_sort, "%s", buffer);

                                        int length = strlen(buffer);
                                        write(client_fd, &length, sizeof(int));
                                        write(client_fd, buffer, length * sizeof(char));
                                }
                                else{
                                        close(client_fd);
                                        close(fd[UP][W]);
                                        close(fd[DOWN][R]);

                                        dup2(fd[UP][R], STDIN_FILENO);
                                        dup2(fd[DOWN][W], STDOUT_FILENO);

                                        execlp("sort", "sort", NULL);


                                        close(fd[UP][R]);
                                        close(fd[DOWN][W]);

                                        abort();
                                }
                                close (sock_fd);
                                close(client_fd);
                                exit(0);
                        }
                }
        } while(true);
        close(sock_fd);

        return EXIT_SUCCESS;
}

