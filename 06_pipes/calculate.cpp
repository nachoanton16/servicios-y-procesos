#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(){

	char* arg_list[]={
		NULL
	};
	
	int fds[2];
	pid_t pid;

	pipe(fds);

	pid=fork();
	if(!pid){
		close(fds[1]);
		dup2(fds[0], STDIN_FILENO);
		execlp("bc", "bc");
	}
	else{
		FILE *fichero;
		close(fds[0]);
		fichero=fdopen(fds[1], "w");
		fprintf(fichero, "2+2");
		fflush(fichero);
		close(fds[1]);
		waitpid(pid, NULL, 0);
	}


	return EXIT_SUCCESS;
}
